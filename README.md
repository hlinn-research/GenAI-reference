```json:table
{
    "fields": [
        {"key": "category", "label": "Category", "sortable": true},
        {"key": "type", "label": "Type", "sortable": true},
        {"key": "name", "label": "Name", "sortable": true},
        {"key": "description", "label": "Description"},
        {"key": "link", "label": "Link"},
        {"key": "date_added", "label": "Date Added", "sortable": true}
    ],
    "items": [
        {
            "category": "AI Resources",
            "type": "Repository",
            "name": "Awesome-AI",
            "description": "A curated list of awesome AI resources, tools, and applications.",
            "link": "https://github.com/awe50me/Awesome-AI",
            "date_added": "2023-06-16"
        },
        {
            "category": "Generative AI",
            "type": "Repository",
            "name": "awesome-generative-ai",
            "description": "A collection of resources on generative AI, including papers, tools, and projects.",
            "link": "https://github.com/steven2358/awesome-generative-ai",
            "date_added": "2023-05-22"
        },
        {
            "category": "Prompt Engineering",
            "type": "Repository",
            "name": "prompt-hacker-collections",
            "description": "A collection of prompt engineering resources and examples for various AI models.",
            "link": "https://github.com/yunwei37/prompt-hacker-collections",
            "date_added": "2024-06-11"
        },
{
            "category": "AI Resources",
            "type": "Repository",
            "name": "Awesome-AI",
            "description": "A curated list of awesome AI resources, tools, and applications.",
            "link": "https://github.com/awe50me/Awesome-AI",
            "date_added": "2023-06-16"
        },
        {
            "category": "Generative AI",
            "type": "Repository",
            "name": "awesome-generative-ai",
            "description": "A collection of resources on generative AI, including papers, tools, and projects.",
            "link": "https://github.com/steven2358/awesome-generative-ai",
            "date_added": "2023-05-22"
        },
        {
            "category": "Prompt Engineering",
            "type": "Repository",
            "name": "prompt-hacker-collections",
            "description": "A collection of prompt engineering resources and examples for various AI models.",
            "link": "https://github.com/yunwei37/prompt-hacker-collections",
            "date_added": "2024-06-11"
        },
{
            "category": "Learning Resources",
            "type": "Video",
            "name": "Demystifying LLMs and Threats My Journey - CSA (Caleb Sima)",
            "description": "A YouTube video by Caleb Sima discussing LLMs and related threats.",
            "link": "https://www.youtube.com/watch?v=q_gDtOu1_7E",
            "date_added": "2023-10-12"
        },
        {
            "category": "Learning Resources",
            "type": "Repository",
            "name": "prompts-for-edu",
            "description": "A collection of prompts designed to help educators utilize AI in the classroom.",
            "link": "https://github.com/microsoft/prompts-for-edu",
            "date_added": "2024-01-15"
        },
        {
            "category": "Learning Resources",
            "type": "Repository",
            "name": "ml-vulhub",
            "description": "A repository by MITRE ATLAS providing intentionally vulnerable machine learning environments for educational purposes.",
            "link": "https://github.com/mitre-atlas/ml-vulhub",
            "date_added": "2024-04-05"
        },
        {
            "category": "Learning Resources",
            "type": "Video Series",
            "name": "Making Friends with Machine Learning: The Entire Course",
            "description": "A YouTube playlist by Google that offers a comprehensive introduction to machine learning.",
            "link": "https://www.youtube.com/watch?v=1vkb7BCMQd0&list=PLRKtJ4IpxJpDsOT_8YDREJrO8cQUtPUVg&index=2",
            "date_added": "2023-11-30"
        },
{
            "category": "Prompt Injection Primers and Research",
            "type": "Repository",
            "name": "PIPE",
            "description": "Prompt Injection Primer for Engineers.",
            "link": "https://github.com/jthack/PIPE",
            "date_added": "2023-08-25"
        },
        {
            "category": "Prompt Injection Primers and Research",
            "type": "Repository",
            "name": "chatbot-injections-exploits",
            "description": "A collection of chatbot injection exploits.",
            "link": "https://github.com/Cranot/chatbot-injections-exploits",
            "date_added": "2023-07-30"
        },
        {
            "category": "Prompt Injection Primers and Research",
            "type": "Repository",
            "name": "Prompt-Injection-Everywhere",
            "description": "A repository exploring various prompt injection techniques.",
            "link": "https://github.com/TakSec/Prompt-Injection-Everywhere",
            "date_added": "2024-05-15"
        },
        {
            "category": "Prompt Injection Primers and Research",
            "type": "Research Paper",
            "name": "Prompt Injection Attacks",
            "description": "A research paper on prompt injection attacks.",
            "link": "https://arxiv.org/abs/2201.11903",
            "date_added": "2022-01-25"
        },
        {
            "category": "Prompt Injection Primers and Research",
            "type": "Repository",
            "name": "awesome-prompt-injection",
            "description": "A curated list of awesome resources related to prompt injection.",
            "link": "https://github.com/FonduAI/awesome-prompt-injection/",
            "date_added": "2023-10-05"
        },
        {
            "category": "Prompt Injection Primers and Research",
            "type": "Blog",
            "name": "The ELIF5 Guide to Prompt Injection",
            "description": "A comprehensive guide to prompt injection techniques and prevention methods.",
            "link": "https://www.lakera.ai/blog/guide-to-prompt-injection",
            "date_added": "2023-11-10"
        },
        {
            "category": "Prompt Injection Primers and Research",
            "type": "Report",
            "name": "Safety and Security Risks of Generative Artificial Intelligence to 2025 (UK)",
            "description": "A report on the safety and security risks of generative AI.",
            "link": "https://assets.publishing.service.gov.uk/media/653932db80884d0013f71b15/generative-ai-safety-security-risks-2025-annex-b.pdf",
            "date_added": "2024-04-28"
        },
{
            "category": "Finance",
            "type": "Repository",
            "name": "Awesome AI in Finance",
            "description": "A curated list of awesome machine learning strategies and tools in the financial market.",
            "link": "https://github.com/georgezouq/awesome-ai-in-finance",
            "date_added": "2023-09-12"
        },
        {
            "category": "Prompt Engineering",
            "type": "Repository",
            "name": "Fixed Input Parameterization",
            "description": "A GitHub repository focusing on fixed input parameterization for prompt engineering.",
            "link": "https://github.com/unbiarirang/Fixed-Input-Parameterization",
            "date_added": "2024-01-03"
        },
        {
            "category": "Prompt Engineering",
            "type": "Repository",
            "name": "Prompt Engineering Guide",
            "description": "A comprehensive guide to prompt engineering, including techniques, tools, and best practices.",
            "link": "https://github.com/dair.ai/Prompt-Engineering-Guide",
            "date_added": "2024-02-10"
        },
        {
            "category": "Attacks",
            "type": "Blog",
            "name": "Attack Scenarios on Code Interpreter",
            "description": "An analysis of potential attack scenarios on code interpreters.",
            "link": "https://evren.ninja/code-interpreter-security.html",
            "date_added": "2023-11-13"
        },
        {
            "category": "Attacks",
            "type": "Gist",
            "name": "Big list of ML attacks",
            "description": "A gist by @csima listing various machine learning attack vectors.",
            "link": "https://gist.github.com/csima/b5ea16b682f6117c11debee7c40fa8fc",
            "date_added": "2023-11-01"
        },
        {
            "category": "Attacks",
            "type": "Research Paper",
            "name": "Sponge Examples: Energy-Latency Attacks on Neural Networks",
            "description": "A research paper discussing energy-latency attacks on neural networks.",
            "link": "https://arxiv.org/pdf/2006.03463.pdf",
            "date_added": "2024-05-10"
        },
        {
            "category": "Attacks",
            "type": "Research Paper",
            "name": "Truth Serum: Poisoning Machine Learning Models to Reveal Their Secrets",
            "description": "A research paper on poisoning machine learning models to extract confidential information.",
            "link": "https://arxiv.org/pdf/2204.00032.pdf",
            "date_added": "2024-03-22"
        },
        {
            "category": "Attacks",
            "type": "Repository",
            "name": "Abusing ML model file formats to create malware on AI systems: A proof of concept",
            "description": "A proof of concept demonstrating how ML model file formats can be exploited to create malware.",
            "link": "https://github.com/Azure/counterfit/wiki/Abusing-ML-model-file-formats-to-create-malware-on-AI-systems:-A-proof-of-concept",
            "date_added": "2024-01-15"
        },
        {
            "category": "Attacks",
            "type": "Repository",
            "name": "Charcuterie: Code execution techniques for ML",
            "description": "A collection of code execution techniques for machine learning and a sample attack on a blackbox model.",
            "link": "https://github.com/moohax/Charcuterie",
            "date_added": "2024-04-02"
        },
        {
            "category": "Attacks",
            "type": "Research Paper",
            "name": "Universal and transferable adversarial attacks on aligned language models",
            "description": "A research paper on universal and transferable adversarial attacks on language models.",
            "link": "https://arxiv.org/pdf/2307.15043.pdf",
            "date_added": "2023-07-30"
        },
        {
            "category": "Attacks",
            "type": "Research Paper",
            "name": "Evasion attacks against machine learning at test time",
            "description": "A research paper discussing evasion attacks on machine learning models during testing.",
            "link": "https://arxiv.org/pdf/1708.06131.pdf",
            "date_added": "2017-08-15"
        },
        {
            "category": "Research",
            "type": "Research Paper",
            "name": "Extracting Training Data from LLMs",
            "description": "A research paper discussing methods for extracting training data from large language models.",
            "link": "https://arxiv.org/pdf/2012.07805.pdf",
            "date_added": "2020-12-15"
        },
        {
            "category": "Prompt Injection",
            "type": "Repository",
            "name": "Prompt Injection (General)",
            "description": "A GitHub topic page containing various repositories related to prompt injection.",
            "link": "https://github.com/topics/prompt-injection",
            "date_added": "2024-02-01"
        },
        {
            "category": "Prompt Injection",
            "type": "Repository",
            "name": "Inverse Scaling Prompt Injection",
            "description": "A repository exploring tasks where language models get worse as they become better at language modeling.",
            "link": "https://github.com/mivanit/inverse-scaling-prompt-injection",
            "date_added": "2023-05-10"
        },
        {
            "category": "Prompt Injection",
            "type": "Repository",
            "name": "Injection with Special Tokens",
            "description": "A repository demonstrating prompt injection using special tokens.",
            "link": "https://github.com/sunghun7511/chatgpt-prompt-injection",
            "date_added": "2023-06-15"
        },
        {
            "category": "Prompt Injection",
            "type": "Repository",
            "name": "Stored Injection POC",
            "description": "A proof of concept demonstrating stored prompt injection.",
            "link": "https://github.com/JosephTLucas/stored_prompt_injection/blob/main/stored_prompt_injection.ipynb",
            "date_added": "2023-07-20"
        },
        {
            "category": "Privacy",
            "type": "Repository",
            "name": "Inversion Attack: Privacy Recovery",
            "description": "A repository demonstrating inversion attacks for privacy recovery in AI models.",
            "link": "https://github.com/AI-secure/SecretGen",
            "date_added": "2024-02-01"
        },
        {
            "category": "Privacy",
            "type": "Research Paper",
            "name": "Beyond Memorization: Violating Privacy via Inference with Large Language Models",
            "description": "A research paper discussing privacy violations through inference with large language models.",
            "link": "https://arxiv.org/pdf/2310.07298v1.pdf",
            "date_added": "2023-10-15"
        },
        {
            "category": "Research",
            "type": "Collection",
            "name": "The Carlini Collection",
            "description": "A comprehensive collection of adversarial example papers by Nicholas Carlini.",
            "link": "https://nicholas.carlini.com/writing/2019/all-adversarial-example-papers.html",
            "date_added": "2019-05-10"
        },
        {
            "category": "Research",
            "type": "Research Paper",
            "name": "Exploiting Programmatic Behavior of LLMs: Dual-Use Through Standard Security Attacks",
            "description": "A research paper discussing dual-use exploitation of LLMs through standard security attacks.",
            "link": "https://arxiv.org/pdf/2302.05733.pdf",
            "date_added": "2023-02-15"
        },
        {
            "category": "Survey Papers (Academic)",
            "type": "Research Paper",
            "name": "Taxonomy of Machine Learning Safety: A Survey and Primer",
            "description": "A comprehensive survey and primer on machine learning safety.",
            "link": "http://export.arxiv.org/pdf/2106.04823",
            "date_added": "2022-03-01"
        },
        {
            "category": "Survey Papers (Academic)",
            "type": "Research Paper",
            "name": "Security for Machine Learning-based Software Systems: a survey of threats, practices and challenge",
            "description": "A survey on the security of machine learning-based software systems, discussing threats, practices, and challenges.",
            "link": "https://arxiv.org/ftp/arxiv/papers/2201/2201.04736.pdf",
            "date_added": "2022-01-01"
        },
        {
            "category": "Survey Papers (Academic)",
            "type": "Research Paper",
            "name": "Toward Comprehensive Risk Assessments and Assurance of AI-Based Systems",
            "description": "A paper discussing comprehensive risk assessments and assurance of AI-based systems.",
            "link": "https://raw.githubusercontent.com/trailofbits/publications/master/papers/toward_comprehensive_risk_assessments.pdf",
            "date_added": "2023-03-01"
        },
        {
            "category": "Survey Papers (Academic)",
            "type": "Research Paper",
            "name": "Practical Attacks on Machine Learning Systems",
            "description": "A whitepaper on practical attacks on machine learning systems.",
            "link": "https://research.nccgroup.com/2022/07/06/whitepaper-practical-attacks-on-machine-learning-systems/",
            "date_added": "2022-07-01"
        },
        {
            "category": "Risk Databases / Incidents",
            "type": "Website",
            "name": "AI Risk",
            "description": "A comprehensive database of AI risks and incidents.",
            "link": "https://airisk.io",
            "date_added": "2024-02-01"
        },
        {
            "category": "Risk Databases / Incidents",
            "type": "Website",
            "name": "AVIDML",
            "description": "A database of machine learning incidents and risks.",
            "link": "https://avidml.org",
            "date_added": "2024-02-01"
        },
        {
            "category": "Risk Databases / Incidents",
            "type": "Website",
            "name": "Incident Database",
            "description": "A database documenting AI incidents.",
            "link": "https://incidentdatabase.ai",
            "date_added": "2024-02-01"
        },
        {
            "category": "Offensive AI",
            "type": "Repository",
            "name": "Offensive AI Compilation",
            "description": "A compilation of resources covering Offensive AI.",
            "link": "https://github.com/jiep/offensive-ai-compilation",
            "date_added": "2024-02-01"
        },
        {
            "category": "Offensive AI",
            "type": "Website",
            "name": "Offensive ML Framework",
            "description": "The Offensive ML Framework wiki.",
            "link": "https://wiki.offsecml.com/Welcome+to+the+Offensive+ML+Framework",
            "date_added": "2024-02-01"
        },
        {
            "category": "CTFs",
            "type": "Website",
            "name": "GPA CTF",
            "description": "A Capture the Flag (CTF) competition.",
            "link": "https://gpa.43z.one",
            "date_added": "2024-02-01"
        },
        {
            "category": "CTFs",
            "type": "Website",
            "name": "Gandalf CTF",
            "description": "A Capture the Flag (CTF) competition.",
            "link": "https://gandalf.lakera.ai",
            "date_added": "2024-02-01"
        },
        {
            "category": "CTFs",
            "type": "Website",
            "name": "MossCap CTF",
            "description": "A Capture the Flag (CTF) competition.",
            "link": "https://grt.lakera.ai/mosscap",
            "date_added": "2024-02-01"
        },
        {
            "category": "CTFs",
            "type": "Repository",
            "name": "Machine Learning CTF Challenges",
            "description": "A repository of machine learning CTF challenges.",
            "link": "https://github.com/alexdevassy/Machine_Learning_CTF_Challenges",
            "date_added": "2024-02-01"
        },
        {
            "category": "CTFs",
            "type": "Website",
            "name": "Doublespeak Chat",
            "description": "A platform for various CTF challenges.",
            "link": "https://doublespeak.chat",
            "date_added": "2024-02-01"
        },
        {
            "category": "Build",
            "type": "Article",
            "name": "Building LLM Applications for Production",
            "description": "An article by Chip Huyen on building LLM applications for production.",
            "link": "https://huyenchip.com/2023/04/11/llm-engineering.html",
            "date_added": "2023-04-11"
        },
        {
            "category": "Build",
            "type": "Article",
            "name": "LLMs, Embeddings, Context Injection, and Next Generation OER",
            "description": "An article discussing LLMs, embeddings, context injection, and next-generation open educational resources.",
            "link": "https://opencontent.org/blog/archives/7205",
            "date_added": "2023-05-01"
        },
        {
            "category": "Build",
            "type": "YouTube Channel",
            "name": "RabbitHoleSyndrome YouTube Channel",
            "description": "A YouTube channel with extensive content on embeddings, building, and using AI applications.",
            "link": "https://www.youtube.com/@RabbitHoleSyndrome",
            "date_added": "2024-02-01"
        },
        {
            "category": "Build",
            "type": "YouTube Video",
            "name": "Video on embeddings and AI applications",
            "description": "A video on embeddings and AI applications by RabbitHoleSyndrome.",
            "link": "https://www.youtube.com/watch?v=Yhtd7yGGGA",
            "date_added": "2024-02-01"
        },
        {
            "category": "Build",
            "type": "YouTube Video",
            "name": "Another video on embeddings and AI applications",
            "description": "Another video on embeddings and AI applications by RabbitHoleSyndrome.",
            "link": "https://www.youtube.com/watch?v=QdDoFfkVkcw",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Website",
            "name": "AI Standards",
            "description": "A search tool for AI standards.",
            "link": "https://aistandardshub.org/ai-standards-search/?_sfm_overall_status=published",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Website",
            "name": "NIST Crosswalk",
            "description": "NIST AI RMF Knowledge Base Crosswalks.",
            "link": "https://airc.nist.gov/AI_RMF_Knowledge_Base/Crosswalks",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Document",
            "name": "NIST 800-160v1r1 Engineering Trustworthy Secure Systems",
            "description": "A NIST special publication on engineering trustworthy secure systems.",
            "link": "https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-160v1r1.pdf",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Document",
            "name": "NIST AI RMF",
            "description": "The NIST AI Risk Management Framework.",
            "link": "https://nvlpubs.nist.gov/nistpubs/ai/NIST.AI.100-1.pdf",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Document",
            "name": "NIST Taxonomy of AI Risk (Draft)",
            "description": "A draft document on the taxonomy of AI risk by NIST.",
            "link": "https://www.nist.gov/system/files/documents/2021/10/15/taxonomy_AI_risks.pdf",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Blog",
            "name": "CISA Proposed Shared Responsibility Model",
            "description": "A blog post on the proposed shared responsibility model for generative AI by CISA.",
            "link": "https://cloudsecurityalliance.org/blog/2023/07/28/generative-ai-proposed-shared-responsibility-model/",
            "date_added": "2023-07-28"
        },
        {
            "category": "Governance",
            "type": "Document",
            "name": "ENISA Multilayer Framework for Good Cybersecurity Practices for AI",
            "description": "A document by ENISA on good cybersecurity practices for AI.",
            "link": "https://www.enisa.europa.eu/publications/multilayer-framework-for-good-cybersecurity-practices-for-ai",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Repository",
            "name": "OWASP LLM Top 10 analysis mapped to other frameworks",
            "description": "A GitHub repository analyzing the OWASP LLM Top 10 and mapping them to other frameworks.",
            "link": "https://github.com/Bobsimonoff/LLM-4-Applications-Commentary/tree/main/docs/LLM-Top-10-Framework-Mappings",
            "date_added": "2024-02-01"
        },
        {
            "category": "Governance",
            "type": "Document",
            "name": "Anecdote: A comprehensive list of risks, controls and policies to securely integrate Generative AI into your organization",
            "description": "A comprehensive toolkit for securely integrating generative AI into organizations.",
            "link": "https://9105956.fs1.hubspotusercontent-na1.net/hubfs/9105956/The%20anecdotes%20AI%20GRC%20Toolkit.pdf",
            "date_added": "2024-02-01"
        },
        {
            "category": "Model Cards",
            "type": "Website",
            "name": "Model Cards with Google",
            "description": "An overview of model cards by Google.",
            "link": "https://modelcards.withgoogle.com/about",
            "date_added": "2024-02-01"
        },
        {
            "category": "Model Cards",
            "type": "Blog",
            "name": "Hugging Face Model Cards",
            "description": "A blog post by Hugging Face on model cards.",
            "link": "https://huggingface.co/blog/model-cards",
            "date_added": "2024-02-01"
        },
        {
            "category": "Model Cards",
            "type": "Research Paper",
            "name": "Model Cards for Model Reporting",
            "description": "A research paper on model cards for model reporting.",
            "link": "https://arxiv.org/pdf/1810.03993.pdf",
            "date_added": "2024-02-01"
        },
        {
            "category": "Model Cards",
            "type": "Repository",
            "name": "Model Card Template",
            "description": "A GitHub repository containing a model card template.",
            "link": "https://github.com/fau-masters-collected-works-cgarbin/model-card-template",
            "date_added": "2024-02-01"
        },
        {
            "category": "Model Cards",
            "type": "Repository",
            "name": "Model Cards and Datasheets",
            "description": "A GitHub repository on model cards and datasheets.",
            "link": "https://github.com/ivylee/model-cards-and-datasheets",
            "date_added": "2024-02-01"
        },
        {
            "category": "Model Cards",
            "type": "Documentation",
            "name": "AWS Model Cards",
            "description": "Documentation on model cards in AWS SageMaker.",
            "link": "https://docs.aws.amazon.com/sagemaker/latest/dg/model-cards.html",
            "date_added": "2024-02-01"
        },
        {
            "category": "Model Cards",
            "type": "Blog",
            "name": "Meta System Cards",
            "description": "A blog post by Meta on system cards for understanding how AI systems work.",
            "link": "https://ai.meta.com/blog/system-cards-a-new-resource-for-understanding-how-ai-systems-work/",
            "date_added": "2024-02-01"
        },
                {
            "category": "Model Cards",
            "type": "Website",
            "name": "CycloneDX MLBOM",
            "description": "An overview of machine learning Bill of Materials (MLBOM) capabilities by CycloneDX.",
            "link": "https://cyclonedx.org/capabilities/mlbom/",
            "date_added": "2024-02-01"
        },
        {
            "category": "Threat Modeling",
            "type": "Article",
            "name": "Threat Modeling AI Systems: Gavin Klondike",
            "description": "An article discussing threat modeling for AI systems by Gavin Klondike.",
            "link": "https://aivillage.org/large%20language%20models/threat-modeling-llm/",
            "date_added": "2024-02-01"
        },
        {
            "category": "M/SDLC",
            "type": "Website",
            "name": "Supply-chain Levels for Software Artifacts (SLSA)",
            "description": "An overview of supply-chain levels for software artifacts.",
            "link": "https://slsa.dev/",
            "date_added": "2024-02-01"
        },
        {
            "category": "M/SDLC",
            "type": "Document",
            "name": "Securing the ML Lifecycle, Industry IoT Consortium",
            "description": "A document by the Industry IoT Consortium on securing the machine learning lifecycle.",
            "link": "https://www.iiconsortium.org/news-pdf/joi-articles/2022-March-JoI-Securing-the-ML-Lifecycle.pdf",
            "date_added": "2024-02-01"
        },
        {
            "category": "M/SDLC",
            "type": "Research Paper",
            "name": "Integrating Machine Learning With Software Development Lifecycles: Insights From Experts",
            "description": "This paper examines the challenges related to integrating ML development with SDLC models.",
            "link": "https://www.researchgate.net/publication/360318448_Integrating_Machine_Learning_With_Software_Development_Lifecycles_Insights_From_Experts",
            "date_added": "2022-05-11"
        },
        {
            "category": "Defense",
            "type": "Repository",
            "name": "Prompt Injection Mitigations",
            "description": "A GitHub repository focusing on various prompt injection mitigation techniques.",
            "link": "https://github.com/Valhall-ai/prompt-injection-mitigations",
            "date_added": "2024-01-25"
        },
        {
            "category": "Defense",
            "type": "Blog",
            "name": "A Framework to Securely Use LLMs",
            "description": "A blog post discussing a framework for securely using large language models (LLMs).",
            "link": "https://boringappsec.substack.com/p/edition-22-a-framework-to-securely/",
            "date_added": "2024-01-15"
        },
        {
            "category": "Tools",
            "type": "Repository",
            "name": "Open Source Vector Similarity Search for Postgres",
            "description": "A GitHub repository for vector similarity search in PostgreSQL.",
            "link": "https://github.com/pgvector/pgvector",
            "date_added": "2024-01-18"
        },
        {
            "category": "Tools",
            "type": "Website",
            "name": "Compare Two Models Side by Side",
            "description": "A tool to compare two machine learning models side by side.",
            "link": "https://sdk.vercel.ai",
            "date_added": "2024-01-20"
        },
{
            "category": "Build",
            "type": "Repository",
            "name": "OpenAI Evals",
            "description": "A framework for evaluating LLMs and LLM systems with an open-source registry of benchmarks.",
            "link": "https://github.com/openai/evals",
            "date_added": "2024-01-15"
        },
        {
            "category": "Build",
            "type": "Repository",
            "name": "ChatGPT Retrieval Plugin",
            "description": "An open-source retrieval plugin for ChatGPT.",
            "link": "https://github.com/openai/chatgpt-retrieval-plugin",
            "date_added": "2024-01-18"
        },
        {
            "category": "Build",
            "type": "Repository",
            "name": "Triton",
            "description": "An open-source project for high-performance deep learning.",
            "link": "https://github.com/openai/triton",
            "date_added": "2024-01-20"
        },
        {
            "category": "Build",
            "type": "Repository",
            "name": "BabyAGI",
            "description": "A repository for creating autonomous AI agents.",
            "link": "https://github.com/yoheinakajima/babyagi",
            "date_added": "2024-01-22"
        },
        {
            "category": "Build",
            "type": "Repository",
            "name": "AutoGPT",
            "description": "A repository for building AI agents that can perform tasks autonomously.",
            "link": "https://github.com/Significant-Gravitas/AutoGPT",
            "date_added": "2024-01-24"
        },
        {
            "category": "Build",
            "type": "Repository",
            "name": "PrivateGPT",
            "description": "A repository for running GPT models privately on your own infrastructure.",
            "link": "https://github.com/imartinez/privateGPT",
            "date_added": "2024-01-26"
        },
        {
            "category": "Build",
            "type": "Repository",
            "name": "LocalAI",
            "description": "A repository for deploying AI models locally without relying on external APIs.",
            "link": "https://github.com/go-skynet/LocalAI",
            "date_added": "2024-01-28"
        },
        {
            "category": "Build",
            "type": "Repository",
            "name": "MailBuddy",
            "description": "A repository for an AI-powered email assistant.",
            "link": "https://github.com/utyvert/mailbuddy",
            "date_added": "2024-01-30"
        },
        {
            "category": "Build",
            "type": "Website",
            "name": "Like JSFiddle, but for Prompts",
            "description": "An online tool for experimenting with AI prompts.",
            "link": "https://prmpts.ai",
            "date_added": "2024-02-01"
        },
{
            "category": "Prompt Injection Defense",
            "type": "Repository",
            "name": "Rebuff",
            "description": "LLM Prompt Injection Detector",
            "link": "https://github.com/protectai/rebuff",
            "date_added": "2024-01-20"
        },
        {
            "category": "Prompt Injection Defense",
            "type": "Repository",
            "name": "LLM Prompt Injection Filtering",
            "description": "Filtering techniques for LLM prompt injection",
            "link": "https://github.com/derwiki/llm-prompt-injection-filtering",
            "date_added": "2024-01-15"
        },
        {
            "category": "Prompt Injection Defense",
            "type": "Repository",
            "name": "LLM Guard",
            "description": "The Security Toolkit for LLM Interactions",
            "link": "https://github.com/laiyer-ai/llm-guard",
            "date_added": "2024-01-18"
        },
        {
            "category": "Prompt Injection Defense",
            "type": "Website",
            "name": "Arthur AI",
            "description": "AI monitoring and prompt injection defense",
            "link": "https://www.arthur.ai",
            "date_added": "2024-02-01"
        },
        {
            "category": "Prompt Injection Target",
            "type": "Repository",
            "name": "Prompt Injection Playground",
            "description": "A playground for experimenting with prompt injections",
            "link": "https://github.com/svenmorgenrothio/Prompt-Injection-Playground",
            "date_added": "2024-01-25"
        },
        {
            "category": "Prompt Injection Harness",
            "type": "Repository",
            "name": "HouYi",
            "description": "A harness for prompt injection attacks",
            "link": "https://github.com/LLMSecurity/HouYi",
            "date_added": "2024-01-22"
        },
        {
            "category": "Prompt Injection Harness",
            "type": "Repository",
            "name": "PromptMap",
            "description": "Mapping tool for prompt injection",
            "link": "https://github.com/utkusen/promptmap",
            "date_added": "2024-01-30"
        },
        {
            "category": "Assessment",
            "type": "Repository",
            "name": "Counterfit",
            "description": "A generic automation layer for assessing the security of machine learning systems",
            "link": "https://github.com/Azure/counterfit",
            "date_added": "2024-01-20"
        },
        {
            "category": "Assessment",
            "type": "Repository",
            "name": "Garak",
            "description": "Tool to check if an LLM will fail in any way",
            "link": "https://github.com/leondz/garak",
            "date_added": "2024-01-18"
        },
        {
            "category": "Assessment",
            "type": "Repository",
            "name": "Adversarial Robustness Toolbox",
            "description": "Tools to defend and evaluate ML models against adversarial threats",
            "link": "https://github.com/Trusted-AI/adversarial-robustness-toolbox",
            "date_added": "2024-01-26"
        },
        {
            "category": "Assessment",
            "type": "Repository",
            "name": "LLM Testing Findings",
            "description": "Pentest findings for AI/ML",
            "link": "https://github.com/BishopFox/llm-testing-findings",
            "date_added": "2024-01-28"
        },
        {
            "category": "Prompts (Non Offense)",
            "type": "Dataset",
            "name": "Awesome ChatGPT Prompts",
            "description": "A collection of awesome ChatGPT prompts.",
            "link": "https://huggingface.co/datasets/fka/awesome-chatgpt-prompts",
            "date_added": "2023-12-10"
        },
        {
            "category": "Prompts (Non Offense)",
            "type": "Dataset",
            "name": "Midjourney Prompts",
            "description": "A dataset of Midjourney prompts for creative image generation.",
            "link": "https://huggingface.co/datasets/succinctly/midjourney-prompts",
            "date_added": "2023-12-12"
        },
        {
            "category": "Prompts (Non Offense)",
            "type": "Dataset",
            "name": "Stable Diffusion Prompts",
            "description": "A collection of prompts for Stable Diffusion.",
            "link": "https://huggingface.co/datasets/daspartho/stable-diffusion-prompts",
            "date_added": "2023-12-15"
        },
        {
            "category": "Negative Test Prompts",
            "type": "Dataset",
            "name": "Refuse to Answer Prompts",
            "description": "A dataset of prompts designed to make AI refuse to answer.",
            "link": "https://huggingface.co/datasets/notrichardren/refuse-to-answer-prompts",
            "date_added": "2023-12-20"
        },
        {
            "category": "Jailbreak Prompts",
            "type": "Dataset",
            "name": "ChatGPT Jailbreak Prompts",
            "description": "A collection of prompts used to jailbreak ChatGPT.",
            "link": "https://huggingface.co/datasets/rubend18/ChatGPT-Jailbreak-Prompts",
            "date_added": "2023-12-25"
        },
        {
            "category": "Jailbreak Prompts",
            "type": "Website",
            "name": "Jailbreak Chat",
            "description": "A website dedicated to ChatGPT jailbreak prompts.",
            "link": "https://jailbreakchat.com",
            "date_added": "2023-12-30"
        },
        {
            "category": "Jailbreak Prompts",
            "type": "Website",
            "name": "GPT Jailbreak",
            "description": "A compilation of jailbreak prompts for GPT.",
            "link": "http://www.jamessawyer.co.uk/pub/gpt_jb.html",
            "date_added": "2023-12-30"
        },
        {
            "category": "Jailbreak Prompts",
            "type": "Research Paper",
            "name": "Jailbreaking ChatGPT via Prompt Engineering: An Empirical Study",
            "description": "An empirical study on jailbreaking ChatGPT using prompt engineering.",
            "link": "https://arxiv.org/pdf/2305.13860.pdf",
            "date_added": "2023-12-30"
        },
        {
            "category": "Bias / Fairness",
            "type": "Dataset",
            "name": "BOLD",
            "description": "A dataset for measuring bias in language models.",
            "link": "https://huggingface.co/datasets/AlexaAI/bold",
            "date_added": "2023-12-15"
        },
        {
            "category": "Bias / Fairness",
            "type": "Dataset",
            "name": "Speech Fairness Dataset",
            "description": "A dataset for assessing fairness in speech recognition models.",
            "link": "https://ai.meta.com/datasets/speech-fairness-dataset/",
            "date_added": "2023-12-18"
        },
        {
            "category": "Toxicity",
            "type": "Dataset",
            "name": "Real Toxicity Prompts",
            "description": "A dataset of prompts containing toxic language.",
            "link": "https://huggingface.co/datasets/allenai/real-toxicity-prompts",
            "date_added": "2023-12-20"
        },
        {
            "category": "Toxicity",
            "type": "Dataset",
            "name": "Real Toxicity Prompts Severe",
            "description": "A dataset of prompts with severe toxic language.",
            "link": "https://huggingface.co/datasets/PanoEvJ/real-toxicity-prompts-severe0.7",
            "date_added": "2023-12-22"
        },
        {
            "category": "Generate/Run Code",
            "type": "Dataset",
            "name": "LLM Bash",
            "description": "A dataset of prompts for generating and running bash scripts.",
            "link": "https://huggingface.co/datasets/LangChainHub-Prompts/LLM_Bash",
            "date_added": "2023-12-25"
        },
        {
            "category": "Generate/Run Code",
            "type": "Dataset",
            "name": "Awesome ChatGPT Prompts",
            "description": "A collection of awesome ChatGPT prompts.",
            "link": "https://huggingface.co/datasets/agie-ai/awesome-chatgpt-prompts",
            "date_added": "2023-12-28"
        },
        {
            "category": "Generate/Run Code",
            "type": "Dataset",
            "name": "Llama2 SQL Instruct Sys Prompt",
            "description": "A dataset of prompts for generating SQL instructions using Llama2.",
            "link": "https://huggingface.co/datasets/kaxap/llama2-sql-instruct-sys-prompt",
            "date_added": "2023-12-30"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "Prompt Injections",
            "description": "A dataset for testing prompt injections.",
            "link": "https://huggingface.co/datasets/deepset/prompt-injections",
            "date_added": "2023-10-20"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "Prompt Injection Cleaned Dataset",
            "description": "A cleaned dataset for prompt injection research.",
            "link": "https://huggingface.co/datasets/imoxto/prompt_injection_cleaned_dataset",
            "date_added": "2024-01-05"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "Prompt Injection Cleaned Dataset v2",
            "description": "An updated version of the cleaned prompt injection dataset.",
            "link": "https://huggingface.co/datasets/imoxto/prompt_injection_cleaned_dataset-v2",
            "date_added": "2024-01-10"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "Prompt Injection HackAPrompt GPT-3.5",
            "description": "A dataset for prompt injections specifically for GPT-3.5.",
            "link": "https://huggingface.co/datasets/imoxto/prompt_injection_hackaprompt_gpt35",
            "date_added": "2024-01-15"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "Prompt Injection Password or Secret",
            "description": "A dataset focusing on prompt injections that target passwords or secrets.",
            "link": "https://huggingface.co/datasets/cgoosen/prompt_injection_password_or_secret",
            "date_added": "2024-01-20"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "Prompt Injection Duplicate Default",
            "description": "A dataset containing duplicated default prompt injections.",
            "link": "https://huggingface.co/datasets/boardsec/prompt-injection-duplicate-default",
            "date_added": "2024-01-25"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "Prompt Injections",
            "description": "A comprehensive dataset of prompt injections.",
            "link": "https://huggingface.co/datasets/JasperLS/prompt-injections",
            "date_added": "2024-01-30"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "HackAPrompt Playground Submissions",
            "description": "Submissions for the HackAPrompt playground.",
            "link": "https://huggingface.co/datasets/jerpint-org/HackAPrompt-Playground-Submissions",
            "date_added": "2024-02-01"
        },
        {
            "category": "Prompt Injection",
            "type": "Dataset",
            "name": "HackAPrompt AICrowd Submissions",
            "description": "Submissions for the HackAPrompt AICrowd competition.",
            "link": "https://huggingface.co/datasets/jerpint-org/HackAPrompt-AICrowd-Submissions",
            "date_added": "2024-02-01"
        },
        {
            "category": "Prompt Injection",
            "type": "Article",
            "name": "Prompt Injection with Control Characters in ChatGPT",
            "description": "An article discussing prompt injection vulnerabilities using control characters in ChatGPT.",
            "link": "https://dropbox.tech/machine-learning/prompt-injection-with-control-characters-openai-chatgpt-llm",
            "date_added": "2024-02-01"
        },
        {
            "category": "Prompt Injection",
            "type": "Repository",
            "name": "Gandalf Prompt Injection Writeup",
            "description": "A writeup for the Gandalf prompt injection game.",
            "link": "https://github.com/tpai/gandalf-prompt-injection-writeup",
            "date_added": "2023-06-15"
        },
        {
            "category": "Prompt Injection",
            "type": "Repository",
            "name": "Prompt Injection Repository",
            "description": "A repository with various prompt injection techniques and defenses.",
            "link": "https://github.com/Prajwalsrinvas/prompt-injection",
            "date_added": "2024-01-28"
        },
        {
            "category": "AI-based Attack Tools (Voice)",
            "type": "Website",
            "name": "Rask AI",
            "description": "A tool for real-time translation and transcription.",
            "link": "https://www.rask.ai",
            "date_added": "2024-02-01"
        },
        {
            "category": "AI Transparency",
            "type": "Website",
            "name": "The Foundation Model Transparency Index",
            "description": "A comprehensive index on the transparency of foundation models.",
            "link": "https://crfm.stanford.edu/fmti/",
            "date_added": "2024-02-01"
        }

    ]
}
```